import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class UserInterface {
	private Controller controller;

	public UserInterface() {
		this.controller = Controller.getInstance();
	}

	/**
	 * Mit dieser Methode werden die Daten eines Flugzeuges vom User abgefragt und an
	 * den Controller uebergeben.
	 */
	public void flugzeugEintragen() {
		System.out.print("Name: ");
		String name = this.readLine();
		System.out.print("Typ: ");
		String type = this.readLine();
		System.out.print("Sitzplätze: ");
		int seats = this.readInteger(0, 1000);

		this.controller.flugzeugEintragen(type, name, seats);
	}

	/**
	 * Mit dieser Methode werden die Daten eines Fluges vom User abgefragt und an den
	 * Controller uebergeben.
	 */
	public void flugEintragen() {
		System.out.print("Flugnummer:");
		String flugnummer = this.readLine();

		System.out.print("Abflugsort:");
		String abflugsort = this.readLine();

		System.out.print("Ankunftsort:");
		String ankunftsort = this.readLine();

		System.out.print("Geplante Abflugzeit:");
		String geplanteAbflugzeit = this.readLine();

		System.out.print("Geplante Ankunftszeit:");
		String geplanteAnkunftszeit = this.readLine();

		System.out.print("Flugzeug auswählen:");
		Flugzeug selectedAirplane = this.selectObjectByUserChoice(this.controller.getFlugzeuge());

		System.out.print("Flugdatum eingeben:");
		String datum = this.readLine();

		System.out.print("Erwartete Abflugszeit:");
		String erwarteteAbflugszeit = this.readLine();

		System.out.print("Erwartete Ankunftszeit:");
		String erwarteteAnkunftszeit = this.readLine();

		System.out.print("Höchstpreis:");
		int hoechstpreis = this.readInteger(0, 10000);

		Flugbeschreibung flugbeschreibung = this.controller.flugbeschreibungEintragen(flugnummer, abflugsort, ankunftsort, geplanteAbflugzeit, geplanteAnkunftszeit);
		this.controller.flugEintragen(
				flugbeschreibung,
				selectedAirplane,
				datum,
				erwarteteAbflugszeit,
				erwarteteAnkunftszeit,
				hoechstpreis
		);
	}

	/**
	 * Mit dieser Methode werden die Daten eines neuen Kunden vom User abgefragt und
	 * an den Controller uebergeben.
	 */
	public void KundenEintragen() {
		System.out.print("Name: ");
		String name = this.readLine();
		System.out.print("Postleitzahl: ");
		String postleitzahl = this.readLine();
		System.out.print("Straße: ");
		String strasse = this.readLine();
		System.out.print("Staat: ");
		String staat = this.readLine();

		this.controller.kundenEintragen(name, postleitzahl, strasse, staat);
	}

	/**
	 * Mit dieser Methode wird ein Flug fuer einen Kunden gebucht. Dabei koennen sowohl
	 * der Flug als auch der Kunde vom User ausgewaehlt werden.
	 */
	public void flugBuchen() {
		System.out.println("Flug auswählen:");
		Flug flug = this.selectObjectByUserChoice(this.controller.getFluege());
		Kunde kunde = this.kundenAuswaehlen();

		if (flug != null && kunde != null) {
			this.controller.flugBuchen(kunde, flug);
		} else {
			System.out.println("Bei der Buchung ist leider etwas schief gelaufen!");
		}
	}


	/**
	 * Durch Aufruf dieser Methode erscheint das Benutzer-Menue und zwar solange,
	 * bis der User das Programm beendet. Die Auswahl des Benutzers wird abgefragt und
	 * die entsprechenden Methoden aufgerufen.
	 */
	public void startMenue() {
		// TODO Was soll die Funktion machen??
	}

	/**
	 * Mit dieser Methode wird eine Integer-Zahl von stdin eingelesen, die von
	 * lowerlimit bis upperlimit betragen darf.
	 *
	 * @param lowerlimit
	 * @param upperlimit
	 * @return int
	 */
	private int readInteger(int lowerlimit, int upperlimit) {
		while (true) {
			try {
				System.out.print("[" + lowerlimit + "-" + upperlimit + "]: ");
				String input = this.readLine();
				return Math.min(Math.max(Integer.parseInt(input), lowerlimit), upperlimit);
			} catch (NumberFormatException e) {
				System.out.print("Please enter a valid number: ");
			}
		}
	}

	/**
	 * Die Daten eines bestimmten Fluges werden von dieser Methode ausgegeben.
	 */
	public void flugdatenAnsehen() {
		System.out.println("Flug auswählen:");
		Flug flug = this.selectObjectByUserChoice(this.controller.getFluege());

		System.out.println("Beschreibung: " + flug.getBeschreibung().toString());
		System.out.println("Flugzeug: " + flug.getFlieger().toString());
		System.out.println("Höchstpreis: " + flug.getPreis());
		System.out.println("Freie Plätze: " + flug.getFreiePlatze());
		System.out.println("Datum: " + flug.getDatum());
		System.out.println("Erwartete Abflugzeit: " + flug.getErwarteteAbflugszeit());
		System.out.println("Erwartete Ankunftszeit: " + flug.getErwarteteAnkunftszeit());
	}

	/**
	 * Die Daten eines bestimmten Fluges ändern
	 */
	public void flugdatenAendern() {
		System.out.println("Flug auswählen:");
		Flug flug = this.selectObjectByUserChoice(this.controller.getFluege());

		System.out.println("Neue Erwartete Abflugzeit:");
		String erwarteteAbflugzeit = this.readLine();

		System.out.println("Neue Erwartete Ankunftszeit:");
		String erwarteteAnkunftszeit = this.readLine();

		this.controller.flugdatenAendern(flug, erwarteteAbflugzeit, erwarteteAnkunftszeit);
	}

	/**
	 * Mit dieser Methode kann eine Buchung storniert werden. Dazu wird zuerst der
	 * Kunde ausgewaehlt. Anschliessend werden alle Buchungen dieses Kunden angezeigt.
	 * Der User waehlt dann die zu stornierende Buchung.
	 */
	public void buchungStornieren() {
		Kunde k = this.kundenAuswaehlen();

		System.out.println("Buchung auswählen:");
		Buchung buchung = this.selectObjectByUserChoice(this.controller.getBuchungen(k));

		this.controller.buchungStornieren(buchung);
	}

	/**
	 * Mit dieser Methode kann ein Kunde ausgewaehlt werden. Zuerst wird der Name des
	 * Kunden abgefragt und anschliessend alle Kunden dieses Namen angezeigt. Der User
	 * waehlt dann den gewaenschten Kunden.
	 *
	 * @return Kunde
	 */
	private Kunde kundenAuswaehlen() {
		if (this.controller.getKunden().size() == 0) {
			System.out.println("Keine Kunden gefunden!");
			return null;
		}

		System.out.println("Name des Kunden:");
		String name = this.readLine();

		System.out.println("Kunde wählen:");
		return this.selectObjectByUserChoice(this.controller.getKunden(name));
	}

	/**
	 * Mit dieser Methode wird ein String von stdin gelesen.
	 *
	 * @return java.lang.String
	 */
	private String readLine() {
		String value = "\0";
		BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
		try {
			value = inReader.readLine();
		} catch (IOException e) {
		}
		return value.trim();
	}

	// Functions flugAuswaehlen, flugzeugAuswaehlen, flugbeschreibungAuswaehlen and buchungAuswaehlen unneccessary -> see "selectObjectByUserChoice" below
	private <T> T selectObjectByUserChoice(List<T> input) {
		if (input != null && !input.isEmpty()) {
			for (int i = 0; i < input.size(); i++) {
				System.out.println((i + 1) + ":" + input.get(i).toString());
			}

			int selectedIndex = this.readInteger(1, input.size()) - 1;
			return input.get(selectedIndex);
		} else {
			System.out.println("Keine Daten gefunden.");
		}
		return null;
	}

	public void start() {
		Menu menu = new Menu();
		menu.setTitel("FH Flughafen Tower");
		menu.insert('1', "Flugzeug eintragen");
		menu.insert('2', "Flug eintragen");
		menu.insert('3', "Kundendaten eintragen");
		menu.insert('4', "Flug buchen");
		menu.insert('5', "Buchung stornieren");
		menu.insert('6', "Flugdaten ansehen");
		menu.insert('7', "Flugdaten ändern");
		menu.insert('n', "Generate sample data");
		menu.insert('q', "Quit");
		char choice;
		while ((choice = menu.exec()) != 'q') {
			try {
				switch (choice) {
					case '1':
						this.flugzeugEintragen();
						break;
					case '2':
						this.flugEintragen();
						break;
					case '3':
						this.KundenEintragen();
						break;
					case '4':
						this.flugBuchen();
						break;
					case '5':
						this.buchungStornieren();
						break;
					case '6':
						this.flugdatenAnsehen();
						break;
					case '7':
						this.flugdatenAendern();
						break;
					case 'n':
						this.controller.generateSampleData();
						break;
				}
			} catch (ClassCastException e) {
			}
		}
		System.out.println("Program finished");
	}

}
