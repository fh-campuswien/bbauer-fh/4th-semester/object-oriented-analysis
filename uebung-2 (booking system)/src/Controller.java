import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;

public class Controller {

	private static Controller ref;
	private ArrayList<Kunde> kunden;
	private ArrayList<Flug> fluege;
	private ArrayList<Flugzeug> flugzeuge;
	private ArrayList<Buchung> buchungen;
	private ArrayList<Flugbeschreibung> flugbeschreibungen;

	/**
	 * Privater Konstruktor. -> Singleton-Pattern
	 */
	private Controller() {
		this.kunden = new ArrayList<>();
		this.fluege = new ArrayList<>();
		this.flugzeuge = new ArrayList<>();
		this.buchungen = new ArrayList<>();
		this.flugbeschreibungen = new ArrayList<>();
	}

	/**
	 * Returniert immer die selbe Instanz der Klasse Controller. -> Singleton-Pattern
	 *
	 * @return Controller
	 */
	public static synchronized Controller getInstance() {
		if (ref == null) {
			ref = new Controller();
		}

		return ref;
	}

	/**
	 * Gibt den Vector Fluege zurueck.
	 *
	 * @return java.util.Vector
	 */
	public ArrayList<Flug> getFluege() {
		return this.fluege;
	}

	/**
	 * Gibt den Vector Kunden zurueck.
	 *
	 * @return java.util.Vector
	 */
	public ArrayList<Kunde> getKunden() {
		return this.kunden;
	}

	/**
	 * @param k
	 * @param f
	 */
	public void flugBuchen(Kunde k, Flug f) {
		Buchung buchung = new Buchung(k, f, f.getPreis());
		f.decrementFreieplaetze();
		this.buchungen.add(buchung);
	}

	/**
	 * @param b
	 */
	public void buchungStornieren(Buchung b) {
		if (this.buchungen.contains(b)) {
			this.buchungen.remove(this.buchungen.indexOf(b));
			b.getFlug().incrementFreieplaetze();
		}
	}

	/**
	 * Gibt den Vector Flugbeschreibungen zurueck.
	 *
	 * @return java.util.Vector
	 */
	public ArrayList<Flugbeschreibung> getFlugbeschreibungen() {
		return this.flugbeschreibungen;
	}

	/**
	 * Gibt den Vector Flugzeuge zurueck.
	 *
	 * @return java.util.Vector
	 */
	public ArrayList<Flugzeug> getFlugzeuge() {
		return this.flugzeuge;
	}

	/**
	 * Gibt alle Buchungen eines bestimmten Kunden zurueck
	 *
	 * @param k
	 * @return java.util.Vector
	 */
	public ArrayList<Buchung> getBuchungen(Kunde k) {
		ArrayList<Buchung> kundenBuchungen = new ArrayList<>();
		for (Buchung buchung : this.buchungen) {
			if (buchung.getKunde() == k) {
				kundenBuchungen.add(buchung);
			}
		}
		return kundenBuchungen;
	}

	/**
	 * Gibt alle Kunden, die einen bestimmten Namen haben, zurueck.
	 *
	 * @param name
	 * @return java.util.Vector
	 */
	public ArrayList<Kunde> getKunden(String name) {
		ArrayList<Kunde> customerWithName = new ArrayList<>();
		for (Kunde kunde : this.kunden) {
			if (kunde.getName().equalsIgnoreCase(name)) {
				customerWithName.add(kunde);
			}
		}
		return customerWithName;
	}

	/**
	 * @param typ
	 * @param name
	 * @param sitzplaetze
	 */
	public Flugzeug flugzeugEintragen(String typ, String name, int sitzplaetze) {
		Flugzeug flugzeug = new Flugzeug(name, sitzplaetze, typ);
		this.flugzeuge.add(flugzeug);
		return flugzeug;
	}

	/**
	 * @param beschr
	 * @param flieger
	 * @param datum
	 * @param erwarteteAbflugszeit
	 * @param erwarteteAnkunftszeit
	 * @param hoechstpreis
	 */
	public Flug flugEintragen(Flugbeschreibung beschr, Flugzeug flieger, String datum, String erwarteteAbflugszeit, String erwarteteAnkunftszeit, int hoechstpreis) {
		Flug flug = new Flug(beschr, flieger, datum, erwarteteAbflugszeit, erwarteteAnkunftszeit, hoechstpreis);
		this.fluege.add(flug);
		return flug;
	}

	/**
	 * @param name
	 * @param postleitzahl
	 * @param strasse
	 * @param staat
	 */
	public Kunde kundenEintragen(String name, String postleitzahl, String strasse, String staat) {
		Kunde kunde = new Kunde(name, postleitzahl, strasse, staat);
		this.kunden.add(kunde);
		return kunde;
	}

	/**
	 * Aendert die erwartete Abflugs- und Ankunftszeit eines bestimmten Fluges.
	 *
	 * @param f
	 * @param erwarteteAbflugszeit
	 * @param erwarteteAnkunftszeit
	 */
	public void flugdatenAendern(Flug f, String erwarteteAbflugszeit, String erwarteteAnkunftszeit) {
		int index = this.fluege.indexOf(f);

		if (index != -1) {
			Flug flug = this.fluege.get(index);
			flug.setErwarteteAnkunftszeit(erwarteteAnkunftszeit);
			flug.setErwarteteAbflugszeit(erwarteteAbflugszeit);
		}
	}

	/**
	 * @param flugnummer
	 * @param abflugsort
	 * @param ankunftsort
	 * @param geplanteAbflugszeit
	 * @param geplanteAnkunftszeit
	 */
	public Flugbeschreibung flugbeschreibungEintragen(String flugnummer, String abflugsort, String ankunftsort, String geplanteAbflugszeit, String geplanteAnkunftszeit) {
		Flugbeschreibung flugbeschreibung = new Flugbeschreibung(flugnummer, abflugsort, ankunftsort, geplanteAbflugszeit, geplanteAnkunftszeit);
		this.flugbeschreibungen.add(flugbeschreibung);
		return flugbeschreibung;
	}

	public void generateSampleData() {
		System.out.println(this.flugzeugEintragen("Airbus", "A-380", 558));
		System.out.println(this.flugzeugEintragen("Boeing", "Boeing 747", 550));

		System.out.println(this.flugbeschreibungEintragen("VK 2002", "SCHWECHAT", "PARIS CHARLES DE GAULLE", "18:05", "18:05"));
		System.out.println(this.flugbeschreibungEintragen("W6 2859", "SCHWECHAT", "STOCKHOLM NYO", "18:05", "18:05"));
		System.out.println(this.flugbeschreibungEintragen("LH 1241", "SCHWECHAT", "FRANKFURT", "18:10", "18:10"));
		System.out.println(this.flugbeschreibungEintragen("OE 312", "SCHWECHAT", "ROM FCO", "18:15", "18:15"));

		Flugbeschreibung flugbeschreibung;
		Flugzeug flugzeug;

		for (int i = 0; i < 5; i++) {
			flugbeschreibung = this.getFlugbeschreibungen().get((int)(Math.random() * this.getFlugbeschreibungen().size()));
			flugzeug = this.getFlugzeuge().get((int)(Math.random() * this.getFlugzeuge().size()));
			System.out.println(this.flugEintragen(
					flugbeschreibung,
					flugzeug,
					(new Date()).toString(),
					flugbeschreibung.getGeplanteAbflugszeit(),
					flugbeschreibung.getGeplanteAnkunftszeit(),
					(int) (Math.random() * 1000)
			));
		}

		System.out.println(this.kundenEintragen("bernhard", "1234", "Street 1", "AT"));
		System.out.println(this.kundenEintragen("florian", "1234", "Street 2", "AT"));
	}
}
