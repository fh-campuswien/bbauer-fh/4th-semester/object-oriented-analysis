public class Flug {
	private String datum;
	private String erwarteteAbflugszeit;
	private String erwarteteAnkunftszeit;
	private int freiePlaetze = 0;
	private int hoechstpreis;
	private Flugbeschreibung beschreibung;
	private Flugzeug flieger;

	/**
	 * @param beschreibung
	 * @param flieger
	 * @param datum
	 * @param erwarteteAbflugszeit
	 * @param erwarteteAnkunftszeit
	 * @param hoechstpreis
	 */
	public Flug(Flugbeschreibung beschreibung, Flugzeug flieger, String datum, String erwarteteAbflugszeit, String erwarteteAnkunftszeit, int hoechstpreis) {
		this.beschreibung = beschreibung;
		this.flieger = flieger;
		this.datum = datum;
		this.hoechstpreis = hoechstpreis;

		this.freiePlaetze = flieger.getSitzplaetze();

		this.setErwarteteAbflugszeit(erwarteteAbflugszeit);
		this.setErwarteteAnkunftszeit(erwarteteAnkunftszeit);
	}

	/**
	 * @return int
	 */
	public int getFreiePlatze() {
		return this.freiePlaetze;
	}

	/**
	 * Der Preis eines Fluges berechnet sich folgendermassen:
	 * Die ersten 30% der Plaetze kosten 30% des Hoechstpreises, die naechsten 40% kosten
	 * 70% des Hoechstpreises und die restlichen Plaetze 100% des Hoechstpreises.
	 * <p>
	 * z.B.: 100 Plaetze, Hoechstpreis >1000
	 * Plaetze 1-30:     >300
	 * Plaetze 31-70:   >700
	 * Plaetze 71-100: >1000
	 *
	 * @return double
	 */
	public double getPreis() {
		double xPercentOfSeatsBooked = (double)(this.getFlieger().getSitzplaetze() - this.getFreiePlatze()) / this.getFlieger().getSitzplaetze();

		if (xPercentOfSeatsBooked <= 0.3) {
			return this.hoechstpreis * 0.3;
		} else if (xPercentOfSeatsBooked <= 0.7) {
			return this.hoechstpreis * 0.7;
		} else {
			return this.hoechstpreis;
		}
	}

	public void incrementFreieplaetze() {
		this.freiePlaetze++;
	}

	public void decrementFreieplaetze() {
		this.freiePlaetze--;
	}

	/**
	 * @return Flugbeschreibung
	 */
	public Flugbeschreibung getBeschreibung() {
		return this.beschreibung;
	}

	/**
	 * @return Flugzeug
	 */
	public Flugzeug getFlieger() {
		return this.flieger;
	}

	/**
	 * @return java.lang.String
	 */
	public String getErwarteteAnkunftszeit() {
		return this.erwarteteAnkunftszeit;
	}

	/**
	 * @param erwarteteAnkunftszeit
	 */
	public void setErwarteteAnkunftszeit(String erwarteteAnkunftszeit) {
		this.erwarteteAnkunftszeit = erwarteteAnkunftszeit;
	}

	/**
	 * @return java.lang.String
	 */
	public String getErwarteteAbflugszeit() {
		return this.erwarteteAbflugszeit;
	}

	/**
	 * @param erwarteteAbflugszeit
	 */
	public void setErwarteteAbflugszeit(String erwarteteAbflugszeit) {
		this.erwarteteAbflugszeit = erwarteteAbflugszeit;
	}

	/**
	 * @return java.lang.String
	 */
	public String getDatum() {
		return this.datum;
	}

	@Override
	public String toString() {
		return "Flug{" +
				"freiePlaetze=" + freiePlaetze +
				", hoechstpreis=" + hoechstpreis +
				", beschreibung=" + beschreibung +
				'}';
	}
}
