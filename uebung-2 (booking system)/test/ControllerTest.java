import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

class ControllerTest {

	private Controller controller;
	private Flugzeug airplane;
	private Flug flight;
	private Kunde customer;

	@BeforeEach
	void setup() {
		this.controller = Controller.getInstance();
		this.airplane = new Flugzeug("test", 10, "test");
		this.flight = new Flug(
				new Flugbeschreibung("test", "test", "test", "test", "test"),
				airplane,
				"2019-02-27",
				"N/A",
				"N/A",
				100
		);
		this.customer = new Kunde(
				"Bernhard",
				"1234",
				"TestStrasse",
				"AT"
		);
	}

	@AfterEach
	void teardown() {
		try {
			Field field = Controller.class.getDeclaredField("ref");
			field.setAccessible(true);
			field.set("ref", null);
		} catch (Exception e) {}
	}

	@Test
	void flugBuchen() {
		this.controller.flugBuchen(this.customer, this.flight);

		Assertions.assertEquals(9, this.flight.getFreiePlatze(), "it should decrement available seats while booking a flight");
		Assertions.assertEquals(1, this.controller.getBuchungen(this.customer).size(), "it should add a booking to the customer bookings list");
	}

	@Test
	void buchungStornieren() {
		this.controller.flugBuchen(this.customer, this.flight);
		Assertions.assertEquals(9, this.flight.getFreiePlatze(), "it should decrement available seats while booking a flight");
		Assertions.assertEquals(1, this.controller.getBuchungen(this.customer).size(), "it should add a booking to the customer bookings list");

		Buchung b = controller.getBuchungen(this.customer).get(0);
		this.controller.buchungStornieren(b);

		Assertions.assertEquals(10, this.flight.getFreiePlatze(), "it should increment available seats while reverting a booking");
		Assertions.assertEquals(0, this.controller.getBuchungen(this.customer).size(), "it should remove a booking from the customer bookings list");
	}

	@Test
	void getKunden() {
		this.controller.kundenEintragen("Kunde1", "1234", "Test", "AT");
		this.controller.kundenEintragen("Kunde2", "1234", "Test", "AT");
		this.controller.kundenEintragen("Kunde3", "1234", "Test", "AT");

		Assertions.assertEquals(3, this.controller.getKunden().size(), "it should return the complete customer list");
	}

	@Test
	void getKundenByName() {
		this.controller.kundenEintragen("Kunde1", "1234", "Test", "AT");
		this.controller.kundenEintragen("Kunde2", "1234", "Test", "AT");
		this.controller.kundenEintragen("Kunde3", "1234", "Test", "AT");
		this.controller.kundenEintragen("Kunde3", "1234", "Test", "AT");
		this.controller.kundenEintragen("Kunde3", "1234", "Test", "AT");

		Assertions.assertEquals(1, this.controller.getKunden("Kunde1").size(), "customer list should contain single entry for Kunde1");
		Assertions.assertEquals(1, this.controller.getKunden("Kunde2").size(), "customer list should contain single entry for Kunde2");
		Assertions.assertEquals(3, this.controller.getKunden("Kunde3").size(), "customer list should contain 3 entries for Kunde3");
	}

	@Test
	void flugdatenAendern() {
		this.controller.flugEintragen(
				new Flugbeschreibung("a", "b", "c", "AB_PLAN_ORIG", "AN_PLAN_ORIG"),
				this.airplane,
				"2019-02-27",
				"AB_ERWARTET_ORIG",
				"AN_ERWARTET_ORIG",
				100
		);

		Flug insertedFlight = this.controller.getFluege().get(0);
		this.controller.flugdatenAendern(insertedFlight, "AB_ERWARTET_TEST", "AN_ERWARTET_TEST");
		Flug changedFlight = this.controller.getFluege().get(0);

		Assertions.assertEquals("AB_ERWARTET_TEST", changedFlight.getErwarteteAbflugszeit(), "it should edit the flight information");
		Assertions.assertEquals("AN_ERWARTET_TEST", changedFlight.getErwarteteAnkunftszeit(), "it should edit the flight information");
	}
}