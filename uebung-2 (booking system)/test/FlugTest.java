import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlugTest {

	private Flug flight;

	@BeforeEach
	void setup() {
		this.flight = new Flug(
				new Flugbeschreibung("test", "test", "test", "test", "test"),
				new Flugzeug("test", 10, "test"),
				"2019-02-27",
				"N/A",
				"N/A",
				100
		);
	}

	@Test
	void testGetPreisForFirst30Percent() {
		// Test for 0 bookings
		Assertions.assertEquals(30, (int)this.flight.getPreis(), "it should return 30% of max price for the first 30% of customers");
		for (int i=0; i<3; i++) {
			this.flight.decrementFreieplaetze();
		}
		// Test for 3 bookings
		Assertions.assertEquals(30, (int)this.flight.getPreis(), "it should return 30% of max price for the first 30% of customers");
	}

	@Test
	void testGetPreisFor30To70Percent() {
		// Test for 4 bookings
		for (int i=0; i<4; i++) {
			this.flight.decrementFreieplaetze();
		}
		Assertions.assertEquals(70, (int)this.flight.getPreis(), "it should return 100% of max price for the last 30% of customers");

		// Test for 7 bookings
		for (int i=0; i<3; i++) {
			this.flight.decrementFreieplaetze();
		}
		Assertions.assertEquals(70, (int)this.flight.getPreis(), "it should return 100% of max price for the last 30% of customers");
	}

	@Test
	void testGetPreisFor70PlusPercent() {
		// Test for 8 bookings
		for (int i=0; i<8; i++) {
			this.flight.decrementFreieplaetze();
		}
		Assertions.assertEquals(100, (int)this.flight.getPreis(), "it should return 100% of max price for the last 30% of customers");

		// Test for 9 bookings
		this.flight.decrementFreieplaetze();
		Assertions.assertEquals(100, (int)this.flight.getPreis(), "it should return 100% of max price for the last 30% of customers");
	}

}