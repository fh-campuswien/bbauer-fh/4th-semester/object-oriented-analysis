package me.bbauer;

public class Main {

    public static void main(String[] args) {
		System.out.print("ZIP Code 1100: ");
		System.out.println(Address.checkPlz(1100));
		System.out.print("ZIP Code 2000: ");
		System.out.println(Address.checkPlz(2000));
		System.out.print("ZIP Code 3000: ");
		System.out.println(Address.checkPlz(3000));

		System.out.print("Is Email Valid: ");
		System.out.println(Person.isEmailValid("hello@bbauer.me"));
    }
}
