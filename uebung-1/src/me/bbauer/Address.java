package me.bbauer;

public class Address {
	private String street;
	private String streetNumber;
	private int zipCode;
	private String city;

	public static String checkPlz(int zipCode) {
		Character code = String.valueOf(zipCode).charAt(0);

		switch (code) {
			case '1':
				return "Wien";
			case '2':
			case '3':
				return "Niederösterreich";
			case '4':
				return "Oberösterreich";
			case '5':
				return "Salzburg";
			case '6':
				return "Tirol / Vorarlberg";
			case '7':
				return "Burgenland";
			case '8':
				return "Steiermark";
			case '9':
				return "Kärnten";
		}
		return null;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
