package me.bbauer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AddressBook {
	private List<Person> people = new ArrayList<>();

	public List<Person> getPeople() {
		return people;
	}

	public void setPeople(List<Person> people) {
		this.people = people;
	}

	public void add(Person p) {
		this.people.add(p);
	}

	public List<Person> search(String search) {
		List<Person> matchingPeople = new ArrayList<>();
		for (Person p : this.people) {
			if (p.getFirstname() == search || p.getLastname() == search) {
				matchingPeople.add(p);
			}
		}
		return matchingPeople;
	}

	public static boolean hasBirthdayToday(Person p) {
		LocalDate currentDate = LocalDate.now();
		return p.getBirthDate().compareTo(currentDate) == 0;
	}
}
