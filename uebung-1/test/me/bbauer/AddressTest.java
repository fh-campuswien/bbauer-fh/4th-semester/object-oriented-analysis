package me.bbauer;

import junit.framework.Assert;
import org.junit.jupiter.api.Test;

class AddressTest {

	@Test
	void checkPlz() {
		Assert.assertEquals("It should show Wien", "Wien", Address.checkPlz(1100));
		Assert.assertEquals("It should show Niederösterreich", "Niederösterreich", Address.checkPlz(2000));
		Assert.assertEquals("It should show Kärnten", "Kärnten", Address.checkPlz(9000));
	}

}