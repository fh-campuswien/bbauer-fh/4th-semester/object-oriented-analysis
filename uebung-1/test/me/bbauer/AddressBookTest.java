package me.bbauer;

import junit.framework.Assert;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class AddressBookTest {


	@Test
	void search() {
		Person bernhard = new Person();
		Person florian = new Person();
		bernhard.setFirstname("bernhard");
		bernhard.setBirthDate( LocalDate.of(1996, 10, 19));
		florian.setFirstname("florian");
		florian.setBirthDate( LocalDate.of(1993, 12, 7));

		AddressBook addressBook = new AddressBook();
		addressBook.add(bernhard);
		addressBook.add(florian);

		Assert.assertTrue("Should find person bernhard", addressBook.search("bernhard").contains(bernhard));
		Assert.assertTrue("Should find person florian", addressBook.search("florian").contains(florian));
	}

	@Test
	void hasBirthdayToday() {
		Person birthdayToday = new Person();
		birthdayToday.setBirthDate(LocalDate.now());

		Person birthdayNotToday = new Person();
		birthdayNotToday.setBirthDate(LocalDate.now().plusDays(1));

		Assert.assertTrue("Should return true if the persons birthday is today", AddressBook.hasBirthdayToday(birthdayToday));
		Assert.assertFalse("Should return false if the persons birthday is not today", AddressBook.hasBirthdayToday(birthdayNotToday));
	}

	@Test
	void add() {
		Person bernhard = new Person();
		AddressBook addressBook = new AddressBook();
		addressBook.add(bernhard);

		Assert.assertTrue("Should add a person to the address book", addressBook.getPeople().contains(bernhard));
	}
}