package me.bbauer;

import junit.framework.Assert;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class PersonTest {

	@Test
	void isOlderThan() {
		Person bernhard = new Person();
		Person florian = new Person();
		bernhard.setBirthDate( LocalDate.of(1996, 10, 19));
		florian.setBirthDate( LocalDate.of(1993, 12, 7));

		Assert.assertFalse("Should test that Bernhard is not older than Florian", bernhard.isOlderThan(florian));
	}

	@Test
	void isEmailValid() {
		Assert.assertTrue("Should return true if the email is valid", Person.isEmailValid("hello@bbauer.me"));
		Assert.assertFalse("Should return false because @ is missing", Person.isEmailValid("hellobbauer.me"));
	}

}